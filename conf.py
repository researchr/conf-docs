#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os

import recommonmark
from recommonmark.parser import CommonMarkParser
from recommonmark.transform import AutoStructify

# -- General configuration

extensions = [
  'sphinx.ext.todo',
  'sphinx.ext.imgmath'
]
imgmath_image_format = 'svg'
templates_path = ['_templates']
source_parsers = {
  '.md': CommonMarkParser,
}
source_suffix = ['.rst', '.md']
master_doc = 'index'
project = u'conf.researchr.org'
copyright = u'2017, Elmer van Chastelet, Danny Groenewegen, Eelco Visser'
author = u'Elmer van Chastelet, Danny Groenewegen, Eelco Visser'
version = '1'
release = version
language = None
exclude_patterns = ['_build', 'notes', 'include', 'README.md']
todo_include_todos = True


# -- Options for HTML output

# Only import and set the ReadTheDocs theme if we're building docs locally.
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
if not on_rtd:
  import sphinx_rtd_theme
  html_theme = 'sphinx_rtd_theme'
  html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

html_static_path = ['_static']
htmlhelp_basename = 'confresearchrorgdoc'

# -- Options for LaTeX output

latex_documents = [
    (master_doc, 'confresearchrorg.tex', u'conf.researchr.org Documentation',
     u'Elmer van Chastelet, Danny Groenewegen, Eelco Visser', 'manual'),
]

# -- Options for manual page output

man_pages = [
    (master_doc, 'confresearchrorg', u'conf.researchr.org Documentation',
     [author], 1)
]

# -- Options for Texinfo output

texinfo_documents = [
    (master_doc, 'confresearchrorg', u'conf.researchr.org Documentation',
     author, 'confresearchrorg', 'One line description of project.',
     'Miscellaneous'),
]

# -- Options for Epub output

epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright
epub_exclude_files = ['search.html']

# -- Additional Sphinx configuration

def setup(app):
  app.add_config_value('recommonmark_config', {'auto_toc_tree_section': 'Contents'}, 'env')
  app.add_transform(AutoStructify)
