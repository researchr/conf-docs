.. _edit-roles:

Edit Managers and Editors
=========================

Required privilege: **Edition Manager**

As a manager, you can add or remove other managers and editors.
When adding a manager, he/she will automatically be assigned as editor.

To change managers/editors:

- Click the **Access Control** item in the edition menu panel at the left
- There are 2 tabs, **Managers** and **Editors**
- To add someone to a group, begin to type his or her name and click on the name when he/she appears in the suggestions 

.. image:: add-managers.png
  :scale: 30%   