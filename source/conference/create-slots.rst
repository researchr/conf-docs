.. _create-slots:

Create Session Slots (conference-level)
=======================================

At the (main) conference-level, one designs the global program schedule by creating session-slots to be distributed over the available days and rooms.
This process typically entails the design of one conference day for a specific room. The session slots from this day can then be copied to another day or room.

A session slot:

- has a start and end time
- has a location (room)
- will be assigned to a track, and defines the time and location of a session. The more detailed schedule of a session itself (session name, talks etc) is managed at the track-level, see :ref:`Add Program Sessions <program-sessions>`.

Create session-slots for one conference day
-------------------------------------------

- Recommendation is to set up the available `Room(s)` first before creating the session slots. If room-names are not known yet, you may first give them a symbolic name, e.g. "Room 1".
- Open the **Program Schedule** manage-page from the conference-level menu.
- Enter the start and end time, and location for a session in the **Add slot** form, and click **Add**.

    - Repeat this for all sessions for that location that day  
    
.. image:: add-session-slot.png

Duplicate slots to other days/rooms
-----------------------------------

After you added the session slots, the slots for a conference day may look like this:  

.. image:: one-day-schedule.png

If this day-schedule is applicable for other days or other rooms as well, you can copy these slots:

- `optional:` In case there are (many) unrelated session slots shown, you can first filter the view to only contain specific day(s) and/or room(s).
- Select the session slots to copy, or use the select-all checkbox at the top of the first column.
- Now, choose the appropriate batch action at the bottom:

    - select **Copy selected slots to different room**, and pick the room that the new copied session slots should have.
    - select **Copy selected slots to different day (shift days)**, and enter the day offset (based on the original slot dates) you want the new session slots to have.

Edit/Delete session slots
-------------------------

The start/end times and location of each single session slot can be changed from the table by clicking the **Edit**-button.

