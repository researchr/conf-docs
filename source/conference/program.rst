.. _program:

Program Schedule (Workflow)
===========================

The program schedule facility provides the following features:

- Conflict detection during configuration of the schedule, with detection of:

    - overlapping events, scheduled at the same location at the same time
    - tight personal schedules, when a person (author/presenter/chair) is scheduled at 2 locations at the same time or close to each other
    - events or sessions scheduled outside the boundaries of the program
- An interactive program page with tabular and timeline views

    - Filterable by day, room, track, awards, personal program
- Filtered program views embedded on the track, event and venue-room pages
- Creation of personal programs by star-ing events in any program view (both by guest and logged-in visitors)
- iCal export/sync service allowing automatic syncing to your calendar.

Workflow
--------

For a good user experience, it is **strongly recommended** that all tracks from the conference/sub-conferences add their schedule with events (talks/tutorials/keynotes/you name it) to the system. An incomplete program will be worthless for your attendees.

The workflow for creation of a complete conference program is as follows:

1. `Session Slots` are to be constructed at the top-level conference by its editor(s). 

    - Session slots divide a conference day into multiple sessions. E.g. 2 morning and 2 afternoon sessions a day for each available room.
    - Each room will have its own session slots, which can easily be copied to other rooms and/or days.
2.  The created session slots are assigned to tracks (drag-drop interface), also by the top-level conference editor(s)

    - Tracks from the conference itself _and_ tracks from any sub-conference get assigned here. The idea here is that distribution of the tracks into rooms/time-slots is managed from the top-level.
3. Track editors now schedule `Program Sessions`. A track editor distributes the events* (talks/tutorials/...) over (newly) created program sessions, where each program session is scheduled in one of the session slots that were assigned to the track.
4. Within each program session, track editors can adjust order of events and the start/end time of each individual event

\* Typically, accepted papers(/artifacts), which are modelled as events, are already added to a track way before the schedule is created.