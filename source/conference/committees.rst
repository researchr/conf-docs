.. _committees:

Set up Committees
=================

Required privilege: **Edition or Track Editor**

Committees can be added both at the conference and track level. For simple conferences like workshops, with only a single track, committees are typically only added at the track level. Features and setup is the same for both levels, except that a track committee is shown in the track page's sidebar.

Add a committee
---------------

- Click the **Committees** item from the Conference or Track menu.
- In the **New committee**-tab, enter the name of the committee (e.g. "Organizing Committee") and add.

.. image:: add-committee.png

Edit a Committee
----------------

- Open the committee from the tabs in the committee page.
- In the **General Data**-panel, you can change the name and order of appearance when more committees are configured
- To add someone:

    - Open the **Add Member**-panel if not already expanded
    - Start entering his/her name. The system will suggest existing profiles to choose from.
    
        - When choosing someone from the suggestions
        - If someone is not in the system yet, complete the form with his/her name and email.
    - Optional: change the role (e.g.  from "Chair" instead of "Committee Member")
    - Optional: check **show role** to display its role next to someone's name when displayed.

.. image:: add-member.png

About the order of committee members
------------------------------------

Committee members, but also people involved in events, are ordered by the `ordinal number` from low to high, which is configurable per member. The default is set to 5000. When the ordinal number is the same (default), the system orders people by their last name. Also, a separate group of members can be configured with the **Display on top**-option. These members will be listed first, using the same order-logic applied within this group, followed by the rest of the members.

.. image:: organize-members.png