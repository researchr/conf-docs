.. _assign-slots:

Assign Session Slots to Tracks
==============================

When finished adding session slots, one can start assigning the sessions to the tracks. Track organizers can then start their work on the schedule, i.e. naming the sessions and scheduling the talks/events within the sessions (see :ref:`Add Program Sessions <program-sessions>`).

Assign session slots
------------------------------

- Open the **Program Schedule** manage-page from the conference-level menu.
- Open the second tab **Populate Session Slots**
- 2 tables are shown. One with all tracks within the conference (and sub-conferences), and one containing the session slots with all rooms from left to right, and for each room all days with session slots.  
- To assign a session slot to a track, drag the targeted track-block from the upper table to the session slot in the lower table

    - By holding the shift-button, you can copy a track-block from within the session blocks table.
    - Un-assign a track from a session slot by dragging the track-block from the lower table to the to the trash can
    - In case you move a session/swap 2 sessions, which already has/have events scheduled within, the system will move the session(s) including its events to the new time-slot/location. In case the time-span of the new session slot differs from the old one, events will be rescheduled to fit within the new time-span, preserving relative time span between the events.
- Click **Verify & Submit Changes** to apply the changes.

    - A review window will first show you a summary of rescheduled events, in case an already scheduled session is moved to another session slot.

.. image:: populate-session-slots.png