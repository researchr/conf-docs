.. _add-track:

Add a Track
===========

Required privilege: **Edition Manager**

Tracks are the elements that contain the call for contributions, important dates and accepted papers. Tracks may also hold events that have not associated with a call for contributions, e.g. for committee meetings.

In order to add a track to a conference:

- open the **General Data** page of the conference
- go to the **Tracks** tab
- fill in the name of the new track and click **Add new track**

.. image:: add-track.png

After creation, you will be added as manager+editor for the track, which is managable from the **Tracks** menu panel at the left, or using the edit button in the reorder widget.
