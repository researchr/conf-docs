.. _styling:

Display and Web Presence (Styling, Menu Items, Custom Footer Code, Website Meta Info, Google Analytics)
=============================================

.. _display_context:

Display Context
---------------


All main conferences (i.e. conferences without a parent conference) have a `Display Context`. The display context determines:
 - the styling of pages
 - the items to show in the navigation menus of the menubar and footer
 - the URLs (and domain name) of pages

For sub-conferences, like workshops, it's optional to have their own display context. By default, sub-conferences use the display context from its parent conference.
This way, visitors can browse between pages of the main conference and workshop(s) as if it's just one website with easy access to all content of the main and sub-conferences.

However, when a sub-conference/co-located event is an entity on its own, one can choose to let it have its own display context.
This is done under the **Display and Web Presence** section of the sub-conference. Optionally, one can configure to adopt attending information from the parent conference in the menu of the sub-conference.

.. image:: display-context.png
  :scale: 30%

Customized styling
--------------

Conf supports changing the style of pages by providing a custom CSS file. The page elements are mainly using classes from Twitter Bootstrap, for which the styling can be overridden by uploading a custom CSS file. This can be done under the **Custom (logobar) Stylesheet** tab on the **Display and Web Presence** page at the conference level. The uploaded css-file will be included *after* the default bootstrap css files.

.. image:: styling.png
  :scale: 20%

Custom footer HTML code
-----------------------

Custom HTML-code can be added to the footer, done also at the **Header/Footer customization**-page. This part is located at the right of the footer, and can be used to hold additional information like a supporter's logo, social links or any other information. This HTML-code is inserted as-is, so one can also (ab)use it to include javascript code to do some fancier stuff with elements on the page.

Menu items
----------

The navigation menu bar on top is automatically generated and aggregates all browsable content from the Display Context's conference and sub-conferences. It has menu items for:

- **Attending**: with links to the venue(s) and additional attending items.
- **Info Items**: this is an optional menu item that gets displayed when additional info pages are set up.
- **Program**: with links to the overall, per-day and personal programs. Only visible when the program is configured
- **Tracks**: with links to the display context's tracks, and if applicable the tracks from sub-conferences in a second column
- **Organization**: with links to the display context's track- and conference-level committees + committees from sub-conferences if applicable, and the people index
- **Search**
- **Links to past/future editions**

The order and notation of top-level menu headings can be customized under the **Menu Headings** tab on the **Display and Web Presence** page. The order of items within each menu (attending items, additional info pages, tracks and committees) can be customized in the sections where these items are managed/created.

.. image:: navbar.png
  :scale: 60%
  
.. image:: custom-menu.png
  :scale: 30%

Photo Carousel
--------------

A Photo Carousel can be added to the conference homepage and track pages. This carousel is displayed in full width under the navigation bar.
The carousel may contain uploaded photo's, or single-color slides with text only. When uploading pictures, make sure the picture may be used (check its license!), and add attribution information when applicable. 

.. image:: carousel-config.png
  :scale: 30%

Web Presence / Analytics
--------------

The **Web Presence / Analytics** tab let's you customize the website meta-data which determines how websites are presented to search engines and in social media.
It also allows setting up a tracking property for use with Google Analytics.
Configurable fields are:

 - **Title** - used in the `<title>` and `<meta property='og:title'>`-tags
 - **Description** - used in the `<meta name='description'>` and `<meta property='og:description'>`-tags
 - **Link Preview Image** - used in the `<meta property='og:image'>`-tag
 - **Keywords** - used in the `<meta name='keywords'>`-tag


.. image:: web-presence.png
  :scale: 30%