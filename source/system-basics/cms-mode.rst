.. _cms-mode:

CMS-mode: FULL vs SIMPLE
========================

For sub-conferences like workshops, typically it is sufficient to have only a **track page** with all the information about the **call**, **important dates**, the **committee(s)** and **programme**. For this reason, we introduced the **SIMPLE** cms-mode, where the configuration is simplified to only have tracks, and no separate home page, nor committees at the conference level. This reduces complexity in navigation for the visitors (a separate home page will often introduce additional navigation and duplicate content for small sub-conferences).

The CMS-mode can be set at the `General Data` manage-page of the conference.

In **SIMPLE** mode, you get:

- One or more `Tracks` with their respective pages. These will be the landing pages for the subconference/workshop/symposium/...
- For each track:

  - a `Call` for contributions
  - one or more `Committees` (e.g. Organizing and Program Committee)
  - ability to import/update items from `HotCRP` or `Conference Publishing Consulting <https://www.conference-publishing.com/>`_
  - its program, with `Sessions`, and `Events` (e.g. talk) within a session.
  - ability to add free text items ("Additional Pages")
  - configurable badges to be attached to events/talks
- An `Image gallery` (at the conference level), useful for adding images, but other file types are also supported
- Ability to embed `Sponsors/Supporters` on track pages (configured at the conference level)
- Control over the `Display Context`. Pages of the sub-conference can be viewed from the parent's display context (default) or the subconference can have its own, which allows:

  - overriding the default home style by uploading a custom CSS
  - having customized footer content
  - inheriting attending menu items from the parent conference
  - See :ref:`display_context` for more info'


In **FULL** mode, you get everything from SIMPLE mode plus

- A separate *conference* home page, with ability to add:

  - `News items`
  - `Sponsors/Supporters`
  - `Social Widgets`
  - `Photo Carousel`
- Ability to add more `Venue`
- Ability to add `Attending Information` items
- `Committees` at the conference level

.. image:: cms-mode.png
