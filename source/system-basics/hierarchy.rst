.. _hierarchy:

Hierarchy
=========

In conf.researchr.org, the model of the CMS is **designed to reflect the hierarchy of actual conferences**, with support for simple conferences having a single `call` for papers, and more complex federations of conferences with `subconferences`, workshops and multiple `tracks` per conference.


Taking a conference as starting point:

- a `conference` can have **multiple sub-conferences** (like workshops, symposia, etc), or can be a sub-conference itself
- a `conference` can have **zero or more conference committees** (e.g., organizing, steering, program committee)
- a `conference` can have **a front page** with free text items, a photo carousel, event carousels and a sidebar with news items, supporters, widgets and upcoming important dates (aggregated from its tracks)
- a `conference` can have one or more **venues**, and **attending information** items
- a `conference` has a **program**, with automatically generated tabular and timeline views of the **tracks**, **sessions** and **events** with support for filtering, a personal program, and calendar feed to be imported into one's digital agenda.  
- a `conference` can have **multiple tracks**

	- a `track` can have zero or more **call for contributions**, **important dates** and **committees**
	- a `track` can have zero or more **session slots** defining the time and place of the session(s)
    
		- each `session slot` can be assigned **events** (talks/welcome/keynote/you_name_it)
		- `session slots` are set up at the conference-level where session slots get assigned to the tracks
		
	- a `track` will have **events** with its own rubric like **accepted papers/artifacts/...**
	- a `track` has its own **page** showing the **call** for contributions, **important dates**, its **program**, **accepted papers**, **committee(s)**, and other **free text items**. 