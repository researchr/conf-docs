.. _important-dates:

Add Important Dates
===================

Adding/managing important dates is done from the Calls menu:

.. image:: add-important-date.png 