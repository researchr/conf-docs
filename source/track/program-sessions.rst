.. _program-sessions:

Add Program Sessions
====================

To add program sessions to a track:

- Open **Sessions and Schedule** from the track menu and open the **New Session**-tab
- Enter a name of the session and add the session. The name can later be changed

Configuring a program session:

- In the edit tab of a session you can select the `Session slot`, which defines the time boundary and location of the session

    - In case no session slots appear in the drop down, it seems that the session slots are not yet created by the editors of the (top-level) conference. Please ask the maintainers of the top-level conference to create the session slots and assign them to the track you're editing.
- Select the events (talks/presentations/tutorials/welcome/...) that should be scheduled within the program session.
- Now, **Save** the program session. In case a session-slot is configured, the events will now automatically be assigned an equal amount of time within the time boundaries of the session slot.

.. image:: event-selection.png
  :scale: 30%
- The **Event Planning** panel shows the session-schedule. Here:

    - You can change the start and end-times of the individual events
    - You can change the order of the events using the drag-drop interface (**Populate Event Slots**)

.. image:: event-schedule.png
  :scale: 30%
- **Session chair(s)** can be added in the next panel

.. image:: session-chair.png
  :scale: 30%

Schedule events from other tracks (Shared Schedule)
---------------------------------------------------

In case sessions for Track A will have events from Track B, the session schedule needs to be shared. This can be done under the **Shared Schedule** tab, where you can enable the track(s) for which events should be schedulable in sessions of the currently viewed track.

.. image:: shared-schedule.png
  :scale: 30%