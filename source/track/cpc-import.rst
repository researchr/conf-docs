.. _cpc-import:

Import / Update from CPC (Conference Publishing Consulting)
===========================================================

When your conference has a contract with `Conference Publishing Consulting <https://www.conference-publishing.com/Procedure.html>`_, accepted papers/artefacts can be obtained and used for import into events by providing the conference id used by CPC, something similar to `MYCONF17`.

Import steps:

- Open the `Import Data` section from the track menu
- Expand the `Conference Publishing Consulting` panel
- Fill in the `Conference Publishing Consulting event id` and hit `Import`
- optional: In case multiple calls fall under the CPC event id, choose the one you want to import events from
- The parsed data is show. Here you can `Start conversion/update wizard`, which will guide you through the imported items

    - Review each item. When correct, hit the convert-button to add it to the track events.

When data is updated by CPC, you can repeat above the above steps. The wizard will then match already existing events against the newly imported items. When matched, the wizard presents you an interface where you can merge updates.

.. image:: import-data.png
  :scale: 30%