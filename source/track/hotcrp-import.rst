.. _hotcrp-import:

Import / Update from HotCRP
===========================

Papers (or other artefacts) submitted and accepted in 
`HotCRP <https://hotcrp.com/>`_ can be imported and converted into events to appear as accepted papers(/posters/...), and become schedulable. The title, abstract, authors, affiliations will be imported.

Import steps:

- Export the papers to json-format using the HotCRP instance.
- Open the `Import Data` section from the track menu
- Expand the `HotCRP` panel
- Upload the json-file. After uploading you can review the parsed data
- Start the conversion wizard, which will guide you through the imported items

    - Review each item. When correct, hit the convert-button to add it to the track events.

You can re-upload newer versions of json-exports, the wizard will then match already existing events against the imported items. When matched, the wizard presents you an interface where you can merge changes.

.. image:: import-data.png
  :scale: 30%