.. _scheduling:

Scheduling
==========

A program schedule for a track consists of `Program Sessions`. A session may have a chair (optional), and can have multiple events (papers/tutorials/..) scheduled within.

As described in :ref:`Program Schedule (Workflow) <program>`, a track's program sessions can be `scheduled` as soon as the `Session Slots` are constructed and assigned to the tracks, both done by the editors of the top-level conference. If these sessions slots are missing yet, sessions can still be created and assigned events. However the schedule of the session is then postponed until the session slots are available and assigned to the track.  