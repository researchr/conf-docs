.. _track-content:

Track page and options
======================

Just browse any of the conference websites to get a grasp of what track pages may look like.  

The webpage of a track may have the following elements:

- A description of what the track is about "About" in **General/About/Badges - General Data**
- One or more `Call for Contributions` giving detailed information on the track (and conference) and how to submit a contribution.
- A sidebar with:

  - committee members
  - important dates

- Additional pages, e.g. with information about the paper format etc.
- A list of Sponsors/Supporters

The following elements are added automatically after adding events and sessions to the track:

- A list of accepted papers/posters/...
- A schedule of program sessions, each session having one or more talks/presentations

Shared Tracks (applicable for sub-conferences)
----------------------------------------------
Tracks can be *shared* with sub-conferences. This is useful for tracks that apply to multiple conferences, e.g. a social events track.

Shared tracks are to be embedded by sub-conferences. Control over embedding can be done at both sides (at the shared track and at the sub-conference).
An embedded track will appear in the track menu for a sub-conference, and all views and data of the shared track (e.g. committees, important dates, call for contrib) will become available in the context of the sub-conference.

General Options
---------------
.. image:: track-config.png
  :scale: 30%