.. _add-call:

Add a Call
==========

Required privilege: **Track Manager**

To add a Call for Papers (/Posters/Tutorials/any other type):

- click on the track name from the **Tracks** menu-panel at the left
- in the opened menu, click **Call for Contributions**
- enter a name for the call in the **New Call**-tab, e.g. "Call for Papers" and hit the **Add new**-button
- now you can add the text accompanying the call-for-papers in the **Page content** field of the call

.. image:: add-call.png    