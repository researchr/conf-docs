.. _easychair-import:

Import / Update from EasyChair
===========================

Papers (or other artefacts) submitted and accepted in 
`EasyChair <https://easychair.org/>`_ can be imported and converted into events to appear as accepted papers(/posters/...), and become schedulable. Data can be imported from the `accepted.html` file, combined with the `authors.csv` file with affiliation information (EasyChair Pro/Exec license) or the mailing list (EasyChair free license) without affiliations.


Import steps:

- Open the `Import Data` section from the track menu
- Expand the `EasyChair` panel
- Upload/enter the required files (`accepted.html` and `authors.csv` or mailing list)
- Start the conversion wizard, which will guide you through the imported items

    - Review each item. When correct, hit the convert-button to add it to the track events.

You can re-upload newer versions of the `accepted.html` and authors information, the wizard will then match already existing events against the imported items. When matched, the wizard presents you an interface where you can merge changes.


.. image:: import-data.png
  :scale: 30%