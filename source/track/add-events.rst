.. _add-events:

Add Events (talks/papers/...)
=============================

Required privilege: **Track Editor**

Tracks may have various types of contributions and events associated with it. E.g., accepted papers with talks or posters, or more general events such as an Opening or Keynote.

In conf.researchr.org these are all modeled as `event`. Each event:

- has an abstract/description, and authors/performers
- can be part of the track's schedule
- may appear in the list of accepted papers/posters/... and will be by default
- may get badges/awards assigned (e.g. "Best Paper Award")
- can have embedded media and external links (e.g. a demo video, link to preprint, DOI link)

To add a single event
---------------------

- open the "Events/Talks" page from the track menu panel, and open the "New event"-tab
- fill in the basic event information and hit the "Create"-button which opens the more detailed edit-event page.
- complete the event data by adding authors/performers and more info if applicable. After the event is created it will show up in the accepted list of [papers/artifacts/...], and later in the track and conference schedule when set up.

Authors will be notified and can add/edit content
---------------------------------------------
When adding an event, its author(s)/performer(s) will receive a notification email with the link to the event page. Authors/performers of papers/events are allowed to add/edit the following content of an event:

- Abstract
- (DOI) links to their publication an preprint
- Media (such as slides)
- Type of event (Talk, Poster, Tutorial, ...)

The title, URL key, schedule and authors cannot be edited.

Batch adding events
-------------------

The system also supports batch-adding and batch-update-merging events from supported systems.
Currently supported are HotCRP, EasyChair and `Conference Publishing Consulting <https://www.conference-publishing.com/Procedure.html>`_.