.. _people:

People, Profiles and Users
==========================

- Each person added in some context on conf.researchr.org will become an identity that can be referenced from any (future) conference instance.
- A person is identified by his/her email address.

    - An identity can only be claimed by the owner of that email address by registering (**Sign Up** from the menu bar)
- All persons have a `General Profile` with someone's up-to-date profile data.

    - In case a user claimed this person identity, only he/she may change the data of the general profile
    - In case the identity is not claimed yet, editors of a conference in which this person has an active role may update the data of the general profile as well
- A person may also have `Conference-specific Profile` s:

    - In case a person has an active role in some conference, **a snapshot copy of the `General Profile` is automatically made after the conference ends**. This copy is called a `Conference-specific Profile`. When profile information is shown in the context of that past conference, it will always reflect someone's bio/affiliation at the time/in the context of the conference.
    - Conference specific profiles can also be created before a conference ends. This may be useful when a bio or affiliation should be different from someone's global info for example.
    - Different from global profiles, editors of a conference may change the data of conference-specific profiles, also when the account where the conference-specific profile belongs to is claimed.