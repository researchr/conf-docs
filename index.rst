==================
conf.researchr.org
==================

Federated conferences such as SPLASH are complex organizations composed of many parts (co-located conferences, symposia, and workshops), and are put together by many people and committees.
Developing the website for such a conference requires a considerable effort, and is often reinvented for each edition of a conference using software that provides little to no support for the domain.
Conf.researchr.org is a domain-specific content management system developed to support the production of large conference websites.
Conf.researchr.org is used for the federated conferences of ACM SIGPLAN.

When you're new to conf.researchr.org, we recommend reading :ref:`Hierarchy <hierarchy>`, as it explains the basic model behind the CMS.


 .. toctree::
    :maxdepth: 1
 
    conf.researchr.org <self>

.. toctree::
   :maxdepth: 2
   :caption: System Basics
   
   Hierarchy <source/system-basics/hierarchy.rst>
   CMS-mode: FULL vs SIMPLE <source/system-basics/cms-mode.rst>
   Display Context (styling, menu, footer) <source/system-basics/styling.rst>
   
This section explains how conf.researchr.org is structured.




.. toctree::
   :maxdepth: 2
   :caption: Conference / Workshop Edition
   
   Edit Managers and Editors <source/conference/edit-roles.rst>
   Add a Track <source/conference/add-track.rst>
   Set up Committees <source/conference/committees.rst>
   Program Schedule (Workflow) <source/conference/program.rst>
   Create Session Slots (conference-level) <source/conference/create-slots.rst>
   Assign Session Slots to Tracks <source/conference/assign-slots.rst>

In this section you’ll learn how to set up the presentation and content of a conference instance in conf.researchr.org.


.. toctree::
   :maxdepth: 2
   :caption: Track
   
   Track page and options <source/track/track-content.rst>
   Add a Call <source/track/add-call.rst>
   Add Important Dates <source/track/important-dates.rst>
   Scheduling <source/track/scheduling.rst>
   Add Events (talks/papers/...) <source/track/add-events.rst>
   Import / Update from HotCRP <source/track/hotcrp-import.rst>
   Import / Update from EasyChair <source/track/easychair-import.rst>
   Import / Update from CPC (Conference Publishing Consulting) <source/track/cpc-import.rst>
   Add Program Sessions <source/track/program-sessions.rst>
   
Tracks are the elements that contain the call for contributions, important dates and accepted papers. They can also be used to hold events that have no call/contributions, e.g. for committee meetings.

A track will have its own page, browsable from the navigation bar of the conference website.


.. toctree::
   :maxdepth: 2
   :caption: Profiles

   People, Profiles and Users <source/people/people.rst>